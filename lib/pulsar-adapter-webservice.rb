# encoding: utf-8
require 'sinatra'
require 'json'
require 'pry'
require 'sequel'

set :bind, '46.36.145.7'
set :port, 9193 

# Pulsar web service for data
get '/' do
	db = connect_to_db
	codes = query_pulsar_codes
	db.disconnect
	codes.to_json
end

get '/data' do
	db = connect_to_db
	vals = query_all_data_to_date( params[:date]).all
	db.disconnect

	vals.to_json
end

get '/data/between' do
	db = connect_to_db
	vals = query_all_data_between_dates( params[:date_start], params[:date_end]).all
	vals = vals.select{|v|!v[:DataValue].nan?}
	db.disconnect

	vals.to_json
end

def connect_to_db()
 @db = Sequel.postgres('vodomerka', host:'localhost', user:'postgres', password:'postgres')
end

def query_pulsar_codes
	@db.fetch('SELECT places.plc_id, pltyp.prp_id, places."Name", pltyp."NameGroup" FROM "Tepl"."Places_cnt" as places, "Tepl"."ParamResPlc_cnt" as pltyp WHERE places.plc_id = pltyp.plc_id order by places.plc_id, pltyp.prp_id').all
end

def query_last_data(plc_id, date)
	@db.fetch('SELECT distinct pltyp.prp_id, pltyp.plc_id, pltyp."NameGroup", pltyp."Comment", arc."DataValue", arc.typ_arh, arc."DateValue", plc."Name" From "Tepl"."ParamResPlc_cnt" as pltyp, "Tepl"."Arhiv_cnt" as arc, "Tepl"."Places_cnt" as plc WHERE pltyp.prp_id = arc.pr_id and pltyp.plc_id = ? and arc."DateValue" = ? and plc."plc_id" = ? order by arc."DateValue" desc, arc.typ_arh', plc_id, date, plc_id).all
end

def query_all_data_to_date(date)
sql = 
	<<-SQL
		SELECT distinct pltyp.prp_id, pltyp.plc_id, 
				pltyp."NameGroup", pltyp."Comment", 
				arc."DataValue", arc.typ_arh, arc."DateValue", 
				plc."Name" 

			FROM "Tepl"."ParamResPlc_cnt" as pltyp, 
					 "Tepl"."Arhiv_cnt" as arc, 
					 "Tepl"."Places_cnt" as plc 
					 
			WHERE pltyp.prp_id = arc.pr_id 
				and pltyp.plc_id = plc."plc_id" 
				and arc."DateValue" = ? 
			ORDER BY arc."DateValue" desc, arc.typ_arh
			SQL

		@db.fetch(sql, date)
end

def query_all_data_between_dates(date_start, date_end)
sql = 
	<<-SQL
select "DateValue", "Name", typ_arh, "DataValue" from (
		SELECT distinct pltyp.prp_id, pltyp.plc_id, 
				pltyp."NameGroup", pltyp."Comment", 
				arc."DataValue", arc.typ_arh, arc."DateValue", 
				plc."Name" 
			FROM "Tepl"."ParamResPlc_cnt" as pltyp, 
					 "Tepl"."Arhiv_cnt" as arc, 
					 "Tepl"."Places_cnt" as plc 
			WHERE pltyp.prp_id = arc.pr_id 
				and pltyp.plc_id = plc."plc_id" 
			
) as datea
group by "DateValue", "Name", typ_arh, "DataValue"
having 				
	"DateValue" >= ? 
	and "DateValue" <= ? 
order by typ_arh, "Name", "DateValue"
			SQL

		@db.fetch(sql, date_start, date_end)
end
